# OrchardFedTest

## Installation Guide
This template should help get you started developing with Vue 3 in Vite.

### Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

### Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

### Project Setup

```sh
npm install
```

#### Compile and Hot-Reload for Development

```sh
npm run dev
```

#### Compile and Minify for Production

```sh
npm run build
```

## CODING DECISIONS MADE

### Technology Stacks
I decided to use Vue 3, TailwindCSS, Vite because this is what I'm most comfortable at the moment and this tech stack is very fresh to me.

### Reusable Components
    1. Article.vue
        I specifically created a separate component for this one because I noticed that in the design, the format for the articles look exactly the same so I decided to create a reusable component for this one, traverse through the list of data and dynamically
        place the data to the component using named slots.
    2. Modal.vue
        I decided to create a reusable component for this because I can see that a lot of images use this feature and thought it would
        be more efficient to just call this component using named slots as well.

### Directory Structure
    1. Fonts = assets/fonts
    2. Images = assets/img
    3. Styles = assets/
    4. Common Components = components/common
    5. Components = components/